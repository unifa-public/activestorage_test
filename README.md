# ActiveStorageテスト用サンプルアプリケーション

[ActiveStorage](https://github.com/rails/rails/tree/master/activestorage) を使った簡単なサンプルアプリケーションです。

公式のREADMEの例にあるアバター(avatar)を持つユーザー(User)のCRUDができます。

ローカルで動かすには以下のスクリプトを実行してください。

    $ git clone https://bitbucket.org/unifa-public/activestorage_test.git

    $ cd activestorage_test
    $ bundle install
    $ rails db:migrate

    # Amazon S3を使用する場合、以下を実行してください。ローカルディスクを使用する場合実行は不要です。
    $ export AWS_ACCESS_KEY_ID=AWSのアクセスキー
    $ export AWS_SECRET_ACCESS_KEY=AWSのシークレット
    $ export AWS_S3_BUCKET=バケット名
    $ vim config/environments/development.rb # `config/environments/development.rb` の31行目の `:local` を `:amazon` に変更

    $ rails s -d

    # http://localhost:3000/users にアクセスしてアバター付きでUserを作って見たりしてください
    $ open http://localhost:3000/users
