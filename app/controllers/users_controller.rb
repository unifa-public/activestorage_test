class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  def index
    # N+1問題を発生させないようにするため with_attached_avatar を呼んでおく。
    @users = User.with_attached_avatar.all
  end

  def show
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)

    # @user.save と @user.avatar.attach それぞれで更新するので、トランザクションが必要なはず。
    ActiveRecord::Base.transaction do
      unless @user.save
        render :new and return
      end

      if params[:user][:avatar].present?
        # ここで外部ファイルをアップロード＆blobsとattachmentsテーブルにレコード追加を行う
        @user.avatar.attach(params[:user][:avatar])
      end
    end

    redirect_to @user, notice: 'User was successfully created.'
  end

  def edit
  end

  def update
    @user.assign_attributes(user_params)

    ActiveRecord::Base.transaction do
      unless @user.save
        render :edit and return
      end

      if params[:user][:avatar].present?
        @user.avatar.attach(params[:user][:avatar])
      end
    end

    redirect_to @user, notice: 'User was successfully updated.'
  end

  def destroy
    # 削除時はデフォルトで ActiveStorage::PurgeJob が登録され、レコード、ファイルとも非同期で削除される。
    @user.destroy

    redirect_to users_url, notice: 'User was successfully destroyed.'
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:name, :age)
  end
end
