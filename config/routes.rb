Rails.application.routes.draw do
  resources :users, only: %i(index show new create edit update destroy)
end
